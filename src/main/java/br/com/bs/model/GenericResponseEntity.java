/*
 * Propriedade intelectual BlueShift.
 * Antes de fazer uso desse fonte entre em contato com a equipe BlueShift.
 * http://blueshift.com.br/licenciamento
 */
package br.com.bs.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alancamillo
 */
@XmlRootElement(name = "genericEntity")
public class GenericResponseEntity {
    private boolean success;
    private String msg;
    private String cause;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    @Override
    public String toString() {
        return "GenericResponseEntity{" + "success=" + success + ", msg=" + msg + ", cause=" + cause + '}';
    }
    
}
