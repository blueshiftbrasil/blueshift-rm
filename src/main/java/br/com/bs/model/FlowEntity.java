/*
 * Propriedade intelectual BlueShift.
 * Antes de fazer uso desse fonte entre em contato com a equipe BlueShift.
 * http://blueshift.com.br/licenciamento
 */
package br.com.bs.model;

import java.util.Set;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alancamillo
 */
@XmlRootElement(name = "flowEntity")
public class FlowEntity {
    
    @NotNull
    private String templateId;
    
    @NotNull
    private String flowId;
    private String flowName;
    private String flowComments;
    
    private Set<TemplateParameterEntity> parameters;

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getFlowId() {
        return flowId;
    }

    public void setFlowId(String flowId) {
        this.flowId = flowId;
    }

    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public String getFlowComments() {
        return flowComments;
    }

    public void setFlowComments(String flowComments) {
        this.flowComments = flowComments;
    }

    public Set<TemplateParameterEntity> getParameters() {
        return parameters;
    }

    public void setParameters(Set<TemplateParameterEntity> parameters) {
        this.parameters = parameters;
    }

    @Override
    public String toString() {
        return "FlowEntity{" + "templateId=" + templateId + ", flowId=" + flowId + ", flowName=" + flowName + ", flowComments=" + flowComments + ", parameters=" + parameters + '}';
    }

}
