/*
 * Propriedade intelectual BlueShift.
 * Antes de fazer uso desse fonte entre em contato com a equipe BlueShift.
 * http://blueshift.com.br/licenciamento
 */
package br.com.bs.model;

import java.util.Set;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alancamillo
 */
@XmlRootElement(name = "templatesParameterEntity")
public class TemplateParametersEntity {
    private Set<TemplateParameterEntity> parameters;

    public Set<TemplateParameterEntity> getParameters() {
        return parameters;
    }

    public void setParameters(Set<TemplateParameterEntity> parameters) {
        this.parameters = parameters;
    }

    @Override
    public String toString() {
        return "TemplateParametersEntity{" + "parameters=" + parameters + '}';
    }
    
}
