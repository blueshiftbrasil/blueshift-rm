/*
 * Propriedade intelectual BlueShift.
 * Antes de fazer uso desse fonte entre em contato com a equipe BlueShift.
 * http://blueshift.com.br/licenciamento
 */
package br.com.bs.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alancamillo
 */
@XmlRootElement(name = "templatesParameterEntity")
public class TemplateParameterEntity {
    private String key;
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "TemplateParameterEntity{" + "key=" + key + ", value=" + value + '}';
    }
    
}
