/*
 * Propriedade intelectual BlueShift.
 * Antes de fazer uso desse fonte entre em contato com a equipe BlueShift.
 * http://blueshift.com.br/licenciamento
 */
package br.com.bs.model;

import java.util.Set;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alancamillo
 */
@XmlRootElement(name = "flowEntity")
public class FlowsEntity {
    private Set<FlowEntity> flows;

    public Set<FlowEntity> getFlows() {
        return flows;
    }

    public void setFlows(Set<FlowEntity> flows) {
        this.flows = flows;
    }

    @Override
    public String toString() {
        return "FlowsEntity{" + "flows=" + flows + '}';
    }
    
}
