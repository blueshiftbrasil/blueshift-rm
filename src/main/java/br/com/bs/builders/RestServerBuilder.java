/*
 * BlueShift intelectual property.
 * Contact the BlueShift team before use this source.
 * http://blueshift.com.br/license
 */
package br.com.bs.builders;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;

/**
 *
 * @author author
 */
@Component
public class RestServerBuilder extends RouteBuilder {
    
    @Override
    public void configure() throws Exception {
        
        getContext().getProperties().put("CamelJettyTempDir", "target");
        
        restConfiguration()
//                .component("netty4-http")
                .component("jetty")
                .componentProperty("throwExceptionOnFailure", "false")
                .host("0.0.0.0")
                .bindingMode(RestBindingMode.off)
                .dataFormatProperty("mustBeJAXBElement", "false")
                .enableCORS(true)
                .apiContextPath("/rm-doc")
                    .apiProperty("api.title", "Running Machine API")
                    .apiProperty("api.version", "1.0")
                    .apiProperty("cors", "true")
                .port("{{app.rest-port}}");
        
    }

}
