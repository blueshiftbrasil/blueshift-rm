/*
 * BlueShift intelectual property.
 * Contact the BlueShift team before use this source.
 * http://blueshift.com.br/license
 */
package br.com.bs.builders;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

/**
 *
 * @author author
 */
@Component
public class SchedulerBuilder extends RouteBuilder {
    
    @Override
    public void configure() throws Exception {
        from("quartz2://scheduler/pooling?cron=0+*+*+*+*+?")
                .log(LoggingLevel.INFO , "br.com.bs.rm"
                        , "Running every minute... {{app.redis-ip}}");
    }
    
}
