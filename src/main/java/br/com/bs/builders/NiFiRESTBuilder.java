/*
 * Propriedade intelectual BlueShift.
 * Antes de fazer uso desse fonte entre em contato com a equipe BlueShift.
 * http://blueshift.com.br/licenciamento
 */
package br.com.bs.builders;

import br.com.bs.config.AppConfig;
import br.com.bs.config.DefaultConstants;
import br.com.bs.config.NiFiRESTConstant;
import br.com.bs.exceptions.UploadException;
import br.com.bs.model.FlowEntity;
import br.com.bs.model.FlowsEntity;
import br.com.bs.model.GenericResponseEntity;
import java.net.ConnectException;
import java.net.UnknownHostException;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.model.rest.RestParamType;
import org.apache.nifi.web.api.entity.ProcessGroupEntity;
import org.apache.nifi.web.api.entity.SearchResultsEntity;
import org.apache.nifi.web.api.entity.TemplateEntity;
import org.apache.nifi.web.api.entity.TemplatesEntity;
import org.elasticsearch.client.transport.NoNodeAvailableException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import br.com.bs.exceptions.SearchException;
import br.com.bs.processors.ElasticProcessor;
import br.com.bs.processors.ErrorResponseProcessor;
import br.com.bs.config.ElasticConstants;
import br.com.bs.processors.ApplyMustacheProcessor;
import br.com.bs.processors.DeleteTempFilesProcessor;
import br.com.bs.processors.PrepareProcessGroupInsertProcessor;
import br.com.bs.processors.ProcessMultipartFileProcessor;
import br.com.bs.processors.ReadMultipartFileProcessor;
import br.com.bs.processors.SetFlowTemplateAsBodyProcessor;
import br.com.bs.processors.SetProcessGroupIdAsHeaderProcessor;
import br.com.bs.processors.SetProcessorIdAsHeaderProcessor;
import br.com.bs.processors.SetProcessorListAsBodyProcessor;
import br.com.bs.processors.XMLToJSONProcessor;
import org.apache.nifi.web.api.entity.ProcessorEntity;
import org.springframework.beans.factory.annotation.Autowired;


/**
 *
 * @author alancamillo
 */
@Component
public class NiFiRESTBuilder extends RouteBuilder{
    
    @Autowired
    private AppConfig config;
    
    @Autowired 
    private ElasticProcessor elasticProcessor;
    
    public static final Logger LOGGER =
            LoggerFactory.getLogger(NiFiRESTBuilder.class);

    @Override
    public void configure() throws Exception {
        
        // Em caso de erro ao se comunicar com o NiFi.
        onException(ConnectException.class)
            .handled(true)
            .setHeader(DefaultConstants.EXCEPTION_MESSAGE).constant("Aparentemente o Apache NiFi esta incomunicavel.")
            .process(new ErrorResponseProcessor())
            .marshal().json(JsonLibrary.Gson, true)
        ;
        
        // Em caso do ID de qualquer objeto nao ser localizado no NiFi.
        onException(HttpOperationFailedException.class)
            .handled(true)
            .setHeader(DefaultConstants.EXCEPTION_MESSAGE).simple("Recurso '${header.NiFiResourceId}' não encontrado.")
            .process(new ErrorResponseProcessor())
            .marshal().json(JsonLibrary.Gson, true)
        ;
        
        // Em caso de erro ao fazer upload de arquivos
        onException(UploadException.class)
            .handled(true)
            .setHeader(DefaultConstants.EXCEPTION_MESSAGE).simple("Erro ao efetuar upload de arquivos.")
            .process(new ErrorResponseProcessor())
            .marshal().json(JsonLibrary.Gson, true)
            ;
        
        // Em caso de erro ao recuperar parametros
        onException(SearchException.class)
            .handled(true)
            .setHeader(DefaultConstants.EXCEPTION_MESSAGE).simple("Erro ao recuperar parâmetros do template.")
            .process(new ErrorResponseProcessor())
            .marshal().json(JsonLibrary.Gson, true)
        ;
        
        // Em caso de erro ao se comunicar com o ElasticSearch.
        onException(NoNodeAvailableException.class, UnknownHostException.class)
            .handled(true)
            .setHeader(DefaultConstants.EXCEPTION_MESSAGE).simple("Não foi possível se comunicar com o ElasticSearch.")
            .process(new ErrorResponseProcessor())
            .marshal().json(JsonLibrary.Gson, true)
        ;
                            
        rest("/templates")
            .get()
                .description("Lista os templates cadastrados no NiFi.")
                .param().name(NiFiRESTConstant.QUERY_FROM_NIFI).type(RestParamType.query).description("Se a chamada deve ser feita diretamente no NiFi.").endParam()
                .responseMessage().code(200).message("Lista de templates recuperado.").responseModel(TemplatesEntity.class).endResponseMessage()

                .route()
                    .log(LoggingLevel.DEBUG, DefaultConstants.LOG_NAME_DEFAULT, "Listando templates.")
                    .choice().when(header(NiFiRESTConstant.QUERY_FROM_NIFI).isEqualTo(true))
                        .setHeader(Exchange.HTTP_URI, simple("{{app.nifi.url}}{{app.nifi.template-url}}"))
                        .to("http4://templates-url")
                    .endChoice()
                    .otherwise()
                        .setHeader(ElasticConstants.MODE).constant(ElasticConstants.MODE_SEARCH)
                        .setHeader(ElasticConstants.ELASTIC_SEARCH_TYPE).constant(ElasticConstants.ELASTIC_SEARCH_TYPE_CONFIG)
                        .process(elasticProcessor)
                        .marshal().json(JsonLibrary.Gson, true)
                    .endChoice()
            .endRest()
                
            .get("/{template-id}")
                .description("Detalhamento dos parâmetros cadastrados para o template.")
                .param().name(NiFiRESTConstant.PARAM_TEMPLATE_ID).description("Id do template criado no NiFi.").dataType("String").endParam()
                .responseMessage().code(200).message("Parametros do template recuperado.").responseModel(TemplateEntity.class).endResponseMessage()
                .responseMessage().code(400).message("Template nao encontrado.").endResponseMessage()
                .responseMessage().code(500).message("Erro interno.").responseModel(GenericResponseEntity.class).endResponseMessage()

                .route()
                    .log(LoggingLevel.DEBUG, DefaultConstants.LOG_NAME_DEFAULT, "Listando parametros do template.")
                    .setHeader(NiFiRESTConstant.RESOURCE_ID).header(NiFiRESTConstant.PARAM_TEMPLATE_ID)
                    .setHeader(ElasticConstants.MODE).constant(ElasticConstants.MODE_SEARCH_BY_ID)
                    .setHeader(ElasticConstants.ELASTIC_SEARCH_TYPE).constant(ElasticConstants.ELASTIC_SEARCH_TYPE_CONFIG)
                    .process(elasticProcessor)
            .endRest()
                
            .post()
                .consumes(DefaultConstants.CONTENT_TYPE_MULTIPART)
                .description("Cadastra um novo Template NiFi.")
                .param().name(DefaultConstants.TEMPLATE_FILENAME_CONSTANT).description("Template NiFi.").type(RestParamType.formData).endParam()
                .param().name("descricao").description("Descrição para Template NiFi.").type(RestParamType.formData).dataType("String").endParam()
                .responseMessage().code(200).message("Parametros do template recuperado.").responseModel(TemplateEntity.class).endResponseMessage()
                .responseMessage().code(530).message("Erro ao cadastrar Template.").endResponseMessage()
                .responseMessage().code(500).message("Erro interno.").responseModel(GenericResponseEntity.class).endResponseMessage()
            .route()
                .log(LoggingLevel.DEBUG, DefaultConstants.LOG_NAME_DEFAULT, "Upload template.")

                //Vare o arquivo multipart recebido
                .process(new ReadMultipartFileProcessor())

                //Monta multipart para envio
                .process(new ProcessMultipartFileProcessor())

                //Chama NiFi
                .setHeader(Exchange.CONTENT_TYPE).constant(DefaultConstants.CONTENT_TYPE_MULTIPART)
                .setHeader(Exchange.HTTP_URI, simple("{{app.nifi.url}}{{app.nifi.template-upload}}"))
                .to("http4://templates-url")

                //Recupera XML e transforma em JSON para padronizacao (TEMPLATE)
                .setHeader(NiFiRESTConstant.XML2JSON_TYPE).constant(NiFiRESTConstant.XML2JSON_TEMPLATE)
                .process(new XMLToJSONProcessor()) //Faz SET de RESOURCE_ID
                .marshal().json(JsonLibrary.Gson, true)

                //Apaga arquivos temporarios gerados pelo multipart
                .process(new DeleteTempFilesProcessor())

                // Gera entrada em ElasticSearch
                .log(LoggingLevel.INFO, DefaultConstants.LOG_NAME_DEFAULT, "Adicionando em ElasticSearch...")
                .setHeader(ElasticConstants.MODE).constant(ElasticConstants.MODE_UPSERT)
                .setHeader(ElasticConstants.ELASTIC_SEARCH_TYPE).constant(ElasticConstants.ELASTIC_SEARCH_TYPE_CONFIG)
                .process(elasticProcessor)
                
                .setHeader(Exchange.CONTENT_TYPE).constant(DefaultConstants.CONTENT_TYPE_JSON)
//                .marshal().json(JsonLibrary.Gson, true)
            .endRest()

            .put("/{template-id}")
                .description("Adiciona parâmetros ao Template NiFi previamente cadastrado.")
                .type(TemplateEntity.class)
                .param().name(NiFiRESTConstant.PARAM_TEMPLATE_ID).description("Id do template criado no NiFi.").dataType("String").endParam()
                .param().name("body").type(RestParamType.body).endParam()
                .responseMessage().code(200).message("Parametros do template recuperado.").responseModel(TemplateEntity.class).endResponseMessage()
                .responseMessage().code(530).message("Erro ao cadastrar Template.").endResponseMessage()
                .responseMessage().code(500).message("Erro interno.").responseModel(GenericResponseEntity.class).endResponseMessage()

                .route()
                    .removeHeaders("Camel*")
                //TODO acrescentar validadores de schema JSON
                //https://github.com/networknt/json-schema-validator
                    .setHeader(NiFiRESTConstant.RESOURCE_ID).header(NiFiRESTConstant.PARAM_TEMPLATE_ID)
                    .log(LoggingLevel.INFO, DefaultConstants.LOG_NAME_DEFAULT, "Adicionando em ElasticSearch...")
                    .setHeader(ElasticConstants.MODE).constant(ElasticConstants.MODE_UPSERT)
                    .setHeader(ElasticConstants.ELASTIC_SEARCH_TYPE).constant(ElasticConstants.ELASTIC_SEARCH_TYPE_CONFIG)
                    .process(elasticProcessor)
            .endRest()

            .delete("/{template-id}")
                .description("Apaga Template NiFi previamente cadastrado.")
                .param().name(NiFiRESTConstant.PARAM_TEMPLATE_ID).description("Id do template criado no NiFi.").dataType("String").endParam()
                .responseMessage().code(200).message("Template apagado.").responseModel(TemplateEntity.class).endResponseMessage()
                .responseMessage().code(400).message("Template nao encontrado.").endResponseMessage()
                .responseMessage().code(530).message("Erro ao apagar Template.").endResponseMessage()
                .responseMessage().code(500).message("Erro interno.").responseModel(GenericResponseEntity.class).endResponseMessage()

                .route()
                    .removeHeaders("Camel*")
                    .setHeader(NiFiRESTConstant.RESOURCE_ID).header(NiFiRESTConstant.PARAM_TEMPLATE_ID)
                    .log(LoggingLevel.DEBUG, DefaultConstants.LOG_NAME_DEFAULT, "Apagando parametros do template. ")
                    .setHeader(Exchange.HTTP_URI, simple("{{app.nifi.url}}{{app.nifi.template-delete}}/${header.template-id}"))
                    .setHeader(Exchange.HTTP_METHOD, constant(DefaultConstants.HTTP_METHOD_DELETE))
                    .to("http4://templates-delete")

                    // Removendo ElasticSearch
                    .log(LoggingLevel.INFO, DefaultConstants.LOG_NAME_DEFAULT, "Removendo ElasticSearch...")
                    .setHeader(ElasticConstants.MODE).constant(ElasticConstants.MODE_DELETE)
                    .setHeader(ElasticConstants.ELASTIC_SEARCH_TYPE).constant(ElasticConstants.ELASTIC_SEARCH_TYPE_CONFIG)
                    .process(elasticProcessor)
                    .marshal().json(JsonLibrary.Gson, true)
            .endRest()
            ;
        
        rest("/flows")
            .get()
                .description("Lista todos os flows registrados na plataforma.")
                .responseMessage().code(200).message("Parametros do template recuperado.").responseModel(FlowsEntity.class).endResponseMessage()
                .responseMessage().code(500).message("Erro interno.").responseModel(GenericResponseEntity.class).endResponseMessage()

            .route()
                .log(LoggingLevel.DEBUG, DefaultConstants.LOG_NAME_DEFAULT, "Listando flows.")
                .setHeader(ElasticConstants.MODE).constant(ElasticConstants.MODE_SEARCH)
                .setHeader(ElasticConstants.ELASTIC_SEARCH_TYPE).constant(ElasticConstants.ELASTIC_SEARCH_TYPE_FLOW)
                .process(elasticProcessor)
                .marshal().json(JsonLibrary.Gson)
            .endRest()

            .get("/{flow-id}")
                .description("Recupera o flow registrado na plataforma.")
                .param().name(NiFiRESTConstant.PARAM_FLOW_ID).description("Id do flow.").dataType("String").endParam()
                .responseMessage().code(200).message("Parametros do template recuperado.").responseModel(FlowsEntity.class).endResponseMessage()
                .responseMessage().code(400).message("Flow nao encontrado.").endResponseMessage()
                .responseMessage().code(500).message("Erro interno.").responseModel(GenericResponseEntity.class).endResponseMessage()

            .route()
                .log(LoggingLevel.DEBUG, DefaultConstants.LOG_NAME_DEFAULT, "Listando flow.")
                .setHeader(NiFiRESTConstant.RESOURCE_ID).header(NiFiRESTConstant.PARAM_FLOW_ID)
                .setHeader(ElasticConstants.MODE).constant(ElasticConstants.MODE_SEARCH_BY_ID)
                .setHeader(ElasticConstants.ELASTIC_SEARCH_TYPE).constant(ElasticConstants.ELASTIC_SEARCH_TYPE_FLOW)
                .process(elasticProcessor)
            .endRest()

            .post().type(FlowEntity.class)
                .description("Cadastra um novo flow na plataforma de ingestão.")
                .responseMessage().code(200).message("Flow cadastrado.").responseModel(FlowEntity.class).endResponseMessage()
                .responseMessage().code(500).message("Erro interno.").responseModel(GenericResponseEntity.class).endResponseMessage()

            .route()
                //TODO acrescentar validadores de schema JSON
                //https://github.com/networknt/json-schema-validator
                .log(LoggingLevel.DEBUG, DefaultConstants.LOG_NAME_DEFAULT, "Cadastro de novo flow.")
                .unmarshal().json(JsonLibrary.Gson, FlowEntity.class)
                .setHeader(NiFiRESTConstant.HEADER_FLOW_ENTITY).body()
                
                //Prepara o ProcessGroup para ser criado no Nifi
                .setHeader(NiFiRESTConstant.PROCESS_GROUP_PRINCIPAL, simple(config.getProcessGroupsPrincipal()))
                .process(new PrepareProcessGroupInsertProcessor())
                .marshal().json(JsonLibrary.Gson, ProcessGroupEntity.class)
                
                //Cria o ProcessGroup no NiFi
                .setHeader(Exchange.HTTP_METHOD, constant(DefaultConstants.HTTP_METHOD_POST))
                .setHeader(Exchange.HTTP_URI, simple("{{app.nifi.url}}{{app.nifi.flows-url}}"))
                .to("http4://flows-url")
                
                .unmarshal().json(JsonLibrary.Jackson, ProcessGroupEntity.class)
                
                //Acrescenta ProcessGroup Id como Id do Flow que está sendo criado
                //Acrescenta o ProcessGroup Id no HEADER
                //Acrescenta o ProcessGroup Id no HEADER como RESOURCE_ID para ser inserido no elastic
                .process(new SetProcessGroupIdAsHeaderProcessor())
                
                //Restaura o template no ProcessGroup recem criado
                .process(new SetFlowTemplateAsBodyProcessor())
                .setHeader(Exchange.HTTP_METHOD, constant(DefaultConstants.HTTP_METHOD_POST))
                .setHeader(Exchange.HTTP_URI, simple("{{app.nifi.url}}{{app.nifi.flows-url-template}}${header.NiFiProcessGroupEntityId}/template-instance"))
                .to("http4://flows-url-template")
                
                .unmarshal().json(JsonLibrary.Jackson, org.apache.nifi.web.api.entity.FlowEntity.class)
                
                //Coloca a lista de processor no BODY
                .process(new SetProcessorListAsBodyProcessor())
                
                //Para cada Processor
                .split(body())
                
                    //Remove tags de status dos processors
                    //Acrescenta o Id Processor no HEADER
                    .process(new SetProcessorIdAsHeaderProcessor())
                    .marshal().json(JsonLibrary.Gson, ProcessorEntity.class)
                
                    //Trocar as variáveis mustashe do processor
                    .process(new ApplyMustacheProcessor())
                
                    //Atualiza o Processor no NiFi
                    .setHeader(Exchange.HTTP_METHOD, constant(DefaultConstants.HTTP_METHOD_PUT))
                    .setHeader(Exchange.HTTP_URI, simple("{{app.nifi.url}}{{app.nifi.processors-url}}/${header.NiFiProcessorEntityId}"))
                    .to("http4://processors-url")
                .end()
                
                .setBody().header(NiFiRESTConstant.HEADER_FLOW_ENTITY)
                .marshal().json(JsonLibrary.Gson, FlowEntity.class)
                
                //Grava Flow no elastic
                .setHeader(ElasticConstants.MODE).constant(ElasticConstants.MODE_UPSERT)
                .setHeader(ElasticConstants.ELASTIC_SEARCH_TYPE).constant(ElasticConstants.ELASTIC_SEARCH_TYPE_FLOW)
                .process(elasticProcessor)
            .endRest()

            .put("/{flow-id}").type(FlowEntity.class)
                .description("Adiciona ou atualiza parâmetros Flow cadastrado previamente.")
                .param().name(NiFiRESTConstant.PARAM_FLOW_ID).description("Id do flow criado na plataforma.").dataType("String").endParam()
                .responseMessage().code(200).message("Parametros do flow inseridos ou atualizados.").responseModel(FlowEntity.class).endResponseMessage()
                .responseMessage().code(400).message("Flow nao encontrado.").endResponseMessage()
                .responseMessage().code(500).message("Erro interno.").responseModel(GenericResponseEntity.class).endResponseMessage()

            .route()
                .log(LoggingLevel.DEBUG, DefaultConstants.LOG_NAME_DEFAULT, "Listando parametros do template.")
                .setHeader(NiFiRESTConstant.RESOURCE_ID).header(NiFiRESTConstant.PARAM_FLOW_ID)
            .endRest()

//            .put("/{flow-id}/setup").type(FlowEntity.class)
//                .description("Provisiona Flow no NiFi (Process Group + Template + Parametrizacao).")
//                .param().name(NiFiRESTConstant.PARAM_FLOW_ID).description("Id do flow criado na plataforma.").dataType("String").endParam()
//                .responseMessage().code(200).message("Flow provisionado.").responseModel(FlowEntity.class).endResponseMessage()
//                .responseMessage().code(400).message("Flow nao encontrado.").endResponseMessage()
//                .responseMessage().code(500).message("Erro interno.").responseModel(GenericResponseEntity.class).endResponseMessage()
//
//            .route()
//                .log(LoggingLevel.DEBUG, DefaultConstants.LOG_NAME_DEFAULT, "Listando parametros do template.")
//                .setHeader(NiFiRESTConstant.RESOURCE_ID).header(NiFiRESTConstant.PARAM_FLOW_ID)
//            .endRest()

            .put("/{flow-id}/start").type(FlowEntity.class)
                .description("Inicializa Flow no NiFi (Process Group + Template).")
                .param().name(NiFiRESTConstant.PARAM_FLOW_ID).description("Id do flow criado na plataforma.").dataType("String").endParam()
                .responseMessage().code(200).message("Flow inicializado.").responseModel(FlowEntity.class).endResponseMessage()
                .responseMessage().code(400).message("Flow nao encontrado.").endResponseMessage()
                .responseMessage().code(500).message("Erro interno.").responseModel(GenericResponseEntity.class).endResponseMessage()

            .route()
                .log(LoggingLevel.DEBUG, DefaultConstants.LOG_NAME_DEFAULT, "Iniciando flow.")
                .setHeader(NiFiRESTConstant.RESOURCE_ID).header(NiFiRESTConstant.PARAM_FLOW_ID)
                .setBody(simple("{\"id\":\"${header.NiFiResourceId}\",\"state\":\"RUNNING\"}"))
                .setHeader(Exchange.HTTP_METHOD, constant(DefaultConstants.HTTP_METHOD_PUT))
                .setHeader(Exchange.HTTP_PATH, constant(""))
                .setHeader(Exchange.HTTP_URI, simple("{{app.nifi.url}}{{app.nifi.flow-process-group-url}}${header.NiFiResourceId}"))
                .to("http4://flows-url")
            .endRest()

            .put("/{flow-id}/stop").type(FlowEntity.class)
                .description("Desliga Flow no NiFi (Process Group + Template).")
                .param().name(NiFiRESTConstant.PARAM_FLOW_ID).description("Parando flow.").dataType("String").endParam()
                .responseMessage().code(200).message("Flow desligado.").responseModel(FlowEntity.class).endResponseMessage()
                .responseMessage().code(400).message("Flow nao encontrado.").endResponseMessage()
                .responseMessage().code(500).message("Erro interno.").responseModel(GenericResponseEntity.class).endResponseMessage()

            .route()
                .log(LoggingLevel.DEBUG, DefaultConstants.LOG_NAME_DEFAULT, "Parando flow.")
                .setHeader(NiFiRESTConstant.RESOURCE_ID).header(NiFiRESTConstant.PARAM_FLOW_ID)
                .setBody(simple("{\"id\":\"${header.NiFiResourceId}\",\"state\":\"STOPPED\"}"))
                .setHeader(Exchange.HTTP_METHOD, constant(DefaultConstants.HTTP_METHOD_PUT))
                .setHeader(Exchange.HTTP_PATH, constant(""))
                .setHeader(Exchange.HTTP_URI, simple("{{app.nifi.url}}{{app.nifi.flow-process-group-url}}${header.NiFiResourceId}"))
                .to("http4://flows-url")
            .endRest()


            .delete("/{flow-id}")
                .description("Remove Flow no NiFi (Process Group + Template).")
                .param().name(NiFiRESTConstant.PARAM_FLOW_ID).description("Id do flow criado na plataforma.").dataType("String").endParam()
                .responseMessage().code(200).message("Flow desligado.").responseModel(FlowEntity.class).endResponseMessage()
                .responseMessage().code(400).message("Flow nao encontrado.").endResponseMessage()
                .responseMessage().code(500).message("Erro interno.").responseModel(GenericResponseEntity.class).endResponseMessage()

            .route()
                .log(LoggingLevel.DEBUG, DefaultConstants.LOG_NAME_DEFAULT, "Listando parametros do template.")
                .setHeader(NiFiRESTConstant.RESOURCE_ID).header(NiFiRESTConstant.PARAM_FLOW_ID)
            .endRest()
            ;
        
        rest("/process-group")
            .get()
                .description("Lista todos os NiFi Process Groups.")
                .responseMessage().code(200).message("Lista de NiFi Process Groups.").responseModel(ProcessGroupEntity[].class).endResponseMessage()
                .responseMessage().code(500).message("Erro interno.").responseModel(GenericResponseEntity.class).endResponseMessage()

                .route()
                    .log(LoggingLevel.DEBUG, DefaultConstants.LOG_NAME_DEFAULT, "Listando Process Groups.")
                    .setHeader(Exchange.HTTP_URI, simple("{{app.nifi.url}}{{app.nifi.process-group-url}}"))
                    .to("http4://process-groups")
            .endRest()

            .get("/{process-group-id}")
                .description("Recupera o status de um NiFi Process Group.")
                .param().name(NiFiRESTConstant.PARAM_PROCESS_GROUP_ID).description("Id do Process Group").dataType("String").endParam()
                .responseMessage().code(200).message("Status de um NiFi Process Group recuperado.").responseModel(ProcessGroupEntity.class).endResponseMessage()
                .responseMessage().code(400).message("Process Group nao encontrado.").endResponseMessage()
                .responseMessage().code(500).message("Erro interno.").responseModel(GenericResponseEntity.class).endResponseMessage()

                .route()
                    .log(LoggingLevel.DEBUG, DefaultConstants.LOG_NAME_DEFAULT, "Recuperado Process Group.")
                    .setHeader(NiFiRESTConstant.RESOURCE_ID).header(NiFiRESTConstant.PARAM_PROCESS_GROUP_ID)
                    .setHeader(Exchange.HTTP_PATH, constant(""))
                    .setHeader(Exchange.HTTP_URI, simple("{{app.nifi.url}}{{app.nifi.process-group-status-url}}${header.process-group-id}/status/"))
                    .to("http4://process-group")
            .endRest()
            ;
        
        rest("search")
            .get("/{termo}")
                .description("Busca por Processors de acordo com o termo.")
                .param().name("termo").description("Termo de busca.").dataType("String").endParam()
                .responseMessage().code(200).message("NiFi Processor que corresponda ao termo.").responseModel(SearchResultsEntity.class).endResponseMessage()
                .responseMessage().code(500).message("Erro interno.").responseModel(GenericResponseEntity.class).endResponseMessage()

                .route()
                    .log(LoggingLevel.DEBUG, DefaultConstants.LOG_NAME_DEFAULT, "Buscando Processor ${header.termo}.")
                    .setHeader(Exchange.HTTP_PATH, constant(""))
                    .setHeader(Exchange.HTTP_URI, simple("{{app.nifi.url}}{{app.nifi.processor-search-url}}${header.termo}"))
                    .to("http4://processor-search")
            .endRest();
    }
    
}
