/*
 * BlueShift intelectual property.
 * Contact the BlueShift team before use this source.
 * http://blueshift.com.br/license
 */
package br.com.bs.exceptions;

/**
 *
 * @author André Camillo
 */
public class UploadException extends RuntimeException {

    public UploadException(String message) {
        super(message);
    }

}
