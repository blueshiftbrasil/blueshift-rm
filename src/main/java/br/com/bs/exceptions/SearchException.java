/*
 * BlueShift intelectual property.
 * Contact the BlueShift team before use this source.
 * http://blueshift.com.br/license
 */
package br.com.bs.exceptions;

/**
 *
 * @author André Camillo
 */
public class SearchException extends RuntimeException {

    public SearchException(String message) {
        super(message);
    }

}
