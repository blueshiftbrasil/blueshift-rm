/*
 * Propriedade intelectual BlueShift.
 * Antes de fazer uso desse fonte entre em contato com a equipe BlueShift.
 * http://blueshift.com.br/licenciamento
 */
package br.com.bs.util;

import br.com.bs.config.DefaultConstants;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.apache.commons.fileupload.MultipartStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;

/**
 *
 * @author alancamillo
 */
public class MultipartUtil {

    //LOGGER ENGINE
    public static final org.slf4j.Logger LOGGER =
            LoggerFactory.getLogger(MultipartUtil.class);
    
    public static Map<String, String> process(InputStream input, String boundary) {
        Map <String, String> cache = new HashMap<>();
        try {
            MultipartStream multipartStream
                    = new MultipartStream(input, boundary.getBytes());
            
            
            boolean nextPart = multipartStream.skipPreamble();
            while (nextPart) {
                String header = multipartStream.readHeaders();

                int startPos = StringUtils.indexOf(header, DefaultConstants.ATTRIBUTE_NAME_CONSTANT)+6;
                String name = StringUtils.substring(
                        header
                        , startPos
                        , StringUtils.indexOf(header, "\"", startPos));
                LOGGER.debug("Variavel localizada no arquivo multipart: {}", name);
                
                File tmp = File.createTempFile(
                        UUID.randomUUID().toString()
                        , null
                        , FileUtils.getTempDirectory());
                
                try (OutputStream out = new BufferedOutputStream(new FileOutputStream(tmp))) {
                    multipartStream.readBodyData(out);
                    out.flush();
                }

                nextPart = multipartStream.readBoundary();
                cache.put(name, tmp.getAbsolutePath());
            }
            
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return cache;
    }
    
    public static void clearCache(Map<String, String> cache) {
        Set<Map.Entry<String, String>> entry = cache.entrySet();
        
        entry.stream().forEach((e) -> {
            LOGGER.trace("Removendo {} e o arquivo {}"
                    , new Object[]{e.getKey(), e.getValue()});
            FileUtils.deleteQuietly(new File(e.getValue()));
                    
        });
    }
    
    public static File getFileCache(Map<String, String> cache, String name) {
        return FileUtils.getFile(cache.get(name));
    }
    
    public static InputStream getInputCache(Map<String, String> cache, String name) {
        try {
            return IOUtils.buffer(FileUtils.openInputStream(
                    FileUtils.getFile(cache.get(name))));
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return null;
    }
    
}
