/*
 * BlueShift intelectual property.
 * Contact the BlueShift team before use this source.
 * http://blueshift.com.br/license
 */
package br.com.bs.util;

import br.com.bs.model.GenericResponseEntity;

/**
 *
 * @author André Camillo
 */
public class ResponseUtil {

    public static GenericResponseEntity getResponse(boolean success, String message, Throwable cause) {
        GenericResponseEntity response = new GenericResponseEntity();
        
        response.setSuccess(success);
        response.setMsg(message);
        
        if (cause != null) {
            response.setCause(cause.getMessage());
        }
        
        return response;
    }
}
