/*
 * Propriedade intelectual BlueShift.
 * Antes de fazer uso desse fonte entre em contato com a equipe BlueShift.
 * http://blueshift.com.br/licenciamento
 */
package br.com.bs.config;

/**
 *
 * @author alancamillo
 */
public interface NiFiRESTConstant {

    
    //HEADERS
    String RESOURCE_ID = "NiFiResourceId";
    String RESOURCES_SEARCH = "NiFiResourcesSearch";
    String HEADER_FLOW_ENTITY = "NiFiFlowEntity";
    String HEADER_PROCESSGROUP_ID = "NiFiProcessGroupEntityId";
    String HEADER_PROCESSOR_ID = "NiFiProcessorEntityId";
    String MULTIPART_CACHE = "NiFiMultipartCache";
    String PARAM_TEMPLATE_ID = "template-id";
    String PARAM_FLOW_ID = "flow-id";
    String PARAM_PROCESS_GROUP_ID = "process-group-id";
    String PARAM_SEARCH_TERMO = "termo";
    String PROCESS_GROUP_PRINCIPAL = "process-group-principal";
    
    String QUERY_FROM_NIFI = "_nifi";
    
    String XML2JSON_TYPE = "XML2JSON_TYPE";
    String XML2JSON_TEMPLATE = "XML2JSON_TEMPLATE";
    
}
