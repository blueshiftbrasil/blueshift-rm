/*
 * BlueShift intelectual property.
 * Contact the BlueShift team before use this source.
 * http://blueshift.com.br/license
 */
package br.com.bs.config;

/**
 *
 * @author author
 */
public interface ElasticConstants {
    String ELASTIC_SEARCH_CLUSTER = "cluster.name";
    String ELASTIC_SEARCH_HOST = "node.name";
    String ELASTIC_SEARCH_PORT = "elastic.port";
    
    String ELASTIC_SEARCH_INDEX = "templates";
    String ELASTIC_SEARCH_TYPE = "ELASTIC_SEARCH_TYPE";
    String ELASTIC_SEARCH_TYPE_CONFIG = "config";
    String ELASTIC_SEARCH_TYPE_FLOW = "flows";
    String ELASTIC_SEARCH_ID = "_id";
    
    String MODE = "elastic-mode";
    String MODE_UPSERT = "elastic-upsert";
    String MODE_SEARCH_BY_ID = "elastic-search-by-id";
    String MODE_SEARCH = "elastic-search";
    String MODE_DELETE = "elastic-delete";
}
