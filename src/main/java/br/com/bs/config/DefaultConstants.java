/*
 * BlueShift intelectual property.
 * Contact the BlueShift team before use this source.
 * http://blueshift.com.br/license
 */
package br.com.bs.config;

/**
 *
 * @author André Camillo
 */
public interface DefaultConstants {

    //CONSTANTES UTILITARIAS
    String CONTENT_TYPE_JSON = "application/json";
    String CONTENT_TYPE_MULTIPART = "multipart/form-data";

    String TEMPLATE_FILENAME_CONSTANT = "template";
    String BOUNDARY_CONSTANT = "boundary=";
    String ATTRIBUTE_NAME_CONSTANT = "name=\"";

    String EXCEPTION_MESSAGE = "exception.message";
    
    String HTTP_METHOD_DELETE = "DELETE";
    String HTTP_METHOD_POST = "POST";
    String HTTP_METHOD_PUT = "PUT";
    String HTTP_METHOD_GET = "GET";
    
    String LOG_NAME_DEFAULT = "br.com.bs.rm";
}
