/*
 * BlueShift intelectual property.
 * Contact the BlueShift team before use this source.
 * http://blueshift.com.br/license
 */
package br.com.bs.config;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import javax.annotation.PostConstruct;
import javax.jms.ConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.activemq.spring.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.component.jms.JmsConfiguration;
import org.apache.camel.spring.boot.CamelContextConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author author
 */
@Configuration
@ComponentScan(basePackages = {"br.com.bs.rm.processors", "br.com.bs.rm.builders", "br.com.bs.rm.config"})
@ConfigurationProperties(prefix = "app")
public class AppConfig {
    
    //LOGGER ENGINE
    public static final Logger LOGGER =
            LoggerFactory.getLogger(AppConfig.class);
    
    @Autowired
    private CamelContext camelContext;
    
    private String pid;
    private String pidFile;
    
    @Value("${app.elasticsearch.cluster-name}")
    private String elasticCluster;
    
    @Value("${app.elasticsearch.host}")
    private String elasticHost;
    
    @Value("${app.elasticsearch.port}")
    private int elasticPort;
    
    @Value("${app.nifi.process-groups-principal}")
    private String processGroupsPrincipal;
    
    @Value("${app.activemq.broker-url}")
    private String mqBrokerURL;

    @Value("${app.activemq.pool-max-connections}")
    private int mqPoolMaxConnections;

    @Value("${app.activemq.concurrent-consumers}")
    private int mqConcurrentConsumers;

    public String getPidFile() {
        return pidFile;
    }

    public void setPidFile(String pidFile) {
        this.pidFile = pidFile;
    }

    public String getElasticCluster() {
        return elasticCluster;
    }

    public void setElasticCluster(String elasticCluster) {
        this.elasticCluster = elasticCluster;
    }

    public String getElasticHost() {
        return elasticHost;
    }

    public void setElasticHost(String elasticHost) {
        this.elasticHost = elasticHost;
    }

    public int getElasticPort() {
        return elasticPort;
    }

    public void setElasticPort(int elasticPort) {
        this.elasticPort = elasticPort;
    }

    public String getProcessGroupsPrincipal() {
        return processGroupsPrincipal;
    }

    public void setProcessGroupsPrincipal(String processGroupsPrincipal) {
        this.processGroupsPrincipal = processGroupsPrincipal;
    }
    
    @Bean
    public CamelContextConfiguration contextConfiguration() {
        return new CamelContextConfiguration() {
            @Override
            public void beforeApplicationStart(CamelContext context) {
                LOGGER.trace("beforeApplicationStart...");
            }

            @Override
            public void afterApplicationStart(CamelContext arg0) {
                LOGGER.trace("afterApplicationStart...");
            }
        };
    }
    
    @Bean
    public BrokerService broker() throws IOException {
        BrokerService service = new BrokerService();
        service.setPersistent(true);
        service.setUseShutdownHook(false);
        service.setTransportConnectorURIs(new String[]{this.mqBrokerURL});
        return service;
    }
    
    @Bean
    public ActiveMQComponent activeMQ() {
        ActiveMQComponent component = new ActiveMQComponent();
        component.setConfiguration(jmsConfig());
        return component;
    }

    @Bean
    public ConnectionFactory jmsConnectionFactory() {
        ActiveMQConnectionFactory activeMQFactory = new ActiveMQConnectionFactory();
        activeMQFactory.setBrokerURL(this.mqBrokerURL);
            
        PooledConnectionFactory pooledFactory = new PooledConnectionFactory();
        pooledFactory.setConnectionFactory(activeMQFactory);
        pooledFactory.setMaxConnections(this.mqPoolMaxConnections);

        return pooledFactory;
    }

    @Bean
    public JmsConfiguration jmsConfig() {
        JmsConfiguration configuration = new JmsConfiguration();
        configuration.setConnectionFactory(jmsConnectionFactory());
        configuration.setConcurrentConsumers(this.mqConcurrentConsumers);
        return configuration;
    }

    @PostConstruct
    public void testConnections() {
        LOGGER.trace("Here we can initialize components and open connections.\n"
                + "Class properties are configured in the application.yml file.");
    }
    
    @PostConstruct
    public void getPID() {
        try {
            this.pid = ManagementFactory.getRuntimeMXBean().getName().split("@")[0];

            Path path = Paths.get(pidFile);
            Files.write(path, Arrays.asList(this.pid));

            LOGGER.info("Application PID: {}", this.pid);
        } catch (IOException | RuntimeException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }
    
}
