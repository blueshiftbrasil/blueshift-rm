/*
 * BlueShift intelectual property.
 * Contact the BlueShift team before use this source.
 * http://blueshift.com.br/license
 */
package br.com.bs.processors;

import br.com.bs.config.NiFiRESTConstant;
import br.com.bs.model.FlowEntity;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.nifi.web.api.dto.ProcessGroupDTO;
import org.apache.nifi.web.api.dto.RevisionDTO;
import org.apache.nifi.web.api.entity.ProcessGroupEntity;

/**
 *
 * @author André Camillo
 */
public class PrepareProcessGroupInsertProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        String processGroupPrincipal = exchange.getIn().getHeader(NiFiRESTConstant.PROCESS_GROUP_PRINCIPAL, String.class);
        
        FlowEntity flowEntity
                = exchange.getIn().getBody(FlowEntity.class);

        ProcessGroupEntity processGroupEntity = new ProcessGroupEntity();
        RevisionDTO revision = new RevisionDTO();
        ProcessGroupDTO processGroup = new ProcessGroupDTO();

        //Revision
        revision.setVersion(0L);

        //ProcessGroup
        processGroup.setParentGroupId(processGroupPrincipal);
        processGroup.setName(flowEntity.getFlowName());
        processGroup.setComments(flowEntity.getFlowComments());

        //ProcessGroupEntity
        processGroupEntity.setRevision(revision);
        processGroupEntity.setComponent(processGroup);

        exchange.getIn().setBody(processGroupEntity);
    }

}
