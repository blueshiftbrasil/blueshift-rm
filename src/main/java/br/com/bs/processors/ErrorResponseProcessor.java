/*
 * BlueShift intelectual property.
 * Contact the BlueShift team before use this source.
 * http://blueshift.com.br/license
 */
package br.com.bs.processors;

import br.com.bs.config.DefaultConstants;
import br.com.bs.config.NiFiRESTConstant;
import br.com.bs.model.GenericResponseEntity;
import br.com.bs.util.ResponseUtil;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static br.com.bs.builders.NiFiRESTBuilder.LOGGER;

/**
 *
 * @author André Camillo
 */
public class ErrorResponseProcessor implements Processor {

    public static final Logger LOGGER
            = LoggerFactory.getLogger(ErrorResponseProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        boolean success = false;
        String message = exchange.getIn().getHeader(DefaultConstants.EXCEPTION_MESSAGE, String.class);
        Throwable cause = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Throwable.class);
        
        LOGGER.trace("Trace error.", cause);
        
        GenericResponseEntity response = ResponseUtil.getResponse(success, message,cause);

        exchange.getIn().setHeader(Exchange.CONTENT_TYPE, DefaultConstants.CONTENT_TYPE_JSON);
        exchange.getIn().setBody(response);

        LOGGER.error(response.toString());
    }

}
