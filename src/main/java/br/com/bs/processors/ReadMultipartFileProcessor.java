/*
 * BlueShift intelectual property.
 * Contact the BlueShift team before use this source.
 * http://blueshift.com.br/license
 */
package br.com.bs.processors;

import br.com.bs.config.DefaultConstants;
import br.com.bs.config.NiFiRESTConstant;
import br.com.bs.exceptions.UploadException;
import br.com.bs.util.MultipartUtil;
import java.io.InputStream;
import java.util.Map;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author André Camillo
 */
public class ReadMultipartFileProcessor implements Processor {
    
    public static final Logger LOGGER
            = LoggerFactory.getLogger(ReadMultipartFileProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        String content_type = exchange.getIn().getHeader(Exchange.CONTENT_TYPE, String.class);
        String boundary = content_type.substring(
                content_type.indexOf(DefaultConstants.BOUNDARY_CONSTANT) + 9);
        
        LOGGER.trace("Boundary: {}", boundary);

        InputStream input = exchange.getIn().getBody(InputStream.class);

        Map<String, String> files = MultipartUtil.process(input, boundary);

        if (files == null || files.isEmpty() || !files.containsKey(DefaultConstants.TEMPLATE_FILENAME_CONSTANT)) {
            throw new UploadException("Parâmetro '" + DefaultConstants.TEMPLATE_FILENAME_CONSTANT + "' não encontrado.");
        }

        exchange.getIn().setHeader(NiFiRESTConstant.MULTIPART_CACHE, files);
    }

}
