/*
 * BlueShift intelectual property.
 * Contact the BlueShift team before use this source.
 * http://blueshift.com.br/license
 */
package br.com.bs.processors;

import br.com.bs.config.NiFiRESTConstant;
import br.com.bs.model.FlowEntity;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 *
 * @author André Camillo
 */
public class SetFlowTemplateAsBodyProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        FlowEntity flow = exchange.getIn().getHeader(
                NiFiRESTConstant.HEADER_FLOW_ENTITY, FlowEntity.class);
        
        String template = "{\n"
                + "    \"templateId\": \"" + flow.getTemplateId() + "\",\n"
                + "    \"originX\": 0.0,\n"
                + "    \"originY\": 0.0\n"
                + "}";
        
        exchange.getIn().setBody(template);
    }

}
