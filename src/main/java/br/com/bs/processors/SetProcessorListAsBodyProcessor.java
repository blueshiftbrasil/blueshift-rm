/*
 * BlueShift intelectual property.
 * Contact the BlueShift team before use this source.
 * http://blueshift.com.br/license
 */
package br.com.bs.processors;

import java.util.Iterator;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.nifi.web.api.entity.ProcessorEntity;

/**
 *
 * @author André Camillo
 */
public class SetProcessorListAsBodyProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        org.apache.nifi.web.api.entity.FlowEntity flowEntity
                = exchange.getIn().getBody(org.apache.nifi.web.api.entity.FlowEntity.class);

        Iterator<ProcessorEntity> processors
                = flowEntity.getFlow().getProcessors().iterator();

        exchange.getIn().setBody(processors);
    }

}
