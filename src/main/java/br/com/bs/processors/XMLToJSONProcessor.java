/*
 * BlueShift intelectual property.
 * Contact the BlueShift team before use this source.
 * http://blueshift.com.br/license
 */
package br.com.bs.processors;

import br.com.bs.config.NiFiRESTConstant;
import br.com.bs.exceptions.UploadException;
import java.io.InputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.UnmarshalException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.nifi.web.api.entity.TemplateEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author André Camillo
 */
public class XMLToJSONProcessor implements Processor {

    public static final Logger LOGGER
            = LoggerFactory.getLogger(XMLToJSONProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        
        String xml2jsonType = exchange.getIn().getHeader(
                NiFiRESTConstant.XML2JSON_TYPE, String.class);
        
        JAXBContext jaxb;
        String resourseId = "";
        Object resource = null;
        
        try {
            
            if(NiFiRESTConstant.XML2JSON_TEMPLATE.equals(xml2jsonType)) {
                jaxb = JAXBContext.newInstance(TemplateEntity.class);
                TemplateEntity template = (TemplateEntity) jaxb.createUnmarshaller()
                        .unmarshal(exchange.getIn().getBody(InputStream.class));
                //ID/OBJECT
                resourseId = template.getTemplate().getId();
                resource = template;
            }
            
            exchange.getIn().setHeader(NiFiRESTConstant.RESOURCE_ID, resourseId);
            exchange.getIn().setBody(resource);
        } catch (UnmarshalException ex) {
            throw new UploadException("Template já existe com o nome informado.");
        }
    }

}
