/*
 * BlueShift intelectual property.
 * Contact the BlueShift team before use this source.
 * http://blueshift.com.br/license
 */
package br.com.bs.processors;

import static br.com.bs.builders.NiFiRESTBuilder.LOGGER;
import br.com.bs.config.NiFiRESTConstant;
import br.com.bs.model.FlowEntity;
import br.com.bs.model.TemplateParameterEntity;
import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Set;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 *
 * @author André Camillo
 */
public class ApplyMustacheProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        String processor
                = exchange.getIn().getBody(String.class);

        FlowEntity flow = exchange.getIn().getHeader(
                NiFiRESTConstant.HEADER_FLOW_ENTITY, FlowEntity.class);

        HashMap<String, Object> scopes = new HashMap<>();

        Set<TemplateParameterEntity> parameters = flow.getParameters();
        parameters.stream().forEach((parameter) -> {
            scopes.put(parameter.getKey(), parameter.getValue());
        });

        Writer writer = new StringWriter();
        MustacheFactory mf = new DefaultMustacheFactory();
        Mustache mustache = mf.compile(new StringReader(processor), "processor");

        mustache.execute(writer, scopes);
        writer.flush();

        String newBody = writer.toString();
        LOGGER.trace("Mustache aplicado no processor: \n{}", newBody);

        exchange.getIn().setBody(newBody);
    }

}
