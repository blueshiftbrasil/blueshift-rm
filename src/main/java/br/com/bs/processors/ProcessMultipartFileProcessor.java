/*
 * BlueShift intelectual property.
 * Contact the BlueShift team before use this source.
 * http://blueshift.com.br/license
 */
package br.com.bs.processors;

import br.com.bs.config.DefaultConstants;
import br.com.bs.config.NiFiRESTConstant;
import br.com.bs.util.MultipartUtil;
import java.util.Map;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author André Camillo
 */
public class ProcessMultipartFileProcessor implements Processor {

    public static final Logger LOGGER
            = LoggerFactory.getLogger(ProcessMultipartFileProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        Map<String, String> files
                = exchange.getIn().getHeader(NiFiRESTConstant.MULTIPART_CACHE, Map.class);
        LOGGER.trace("Mutipart content: {}", files);

        MultipartEntityBuilder builder = MultipartEntityBuilder
                .create().addBinaryBody(
                        DefaultConstants.TEMPLATE_FILENAME_CONSTANT,
                         MultipartUtil.getFileCache(files, DefaultConstants.TEMPLATE_FILENAME_CONSTANT));
        
        exchange.getIn().setBody(builder.build());
    }

}
