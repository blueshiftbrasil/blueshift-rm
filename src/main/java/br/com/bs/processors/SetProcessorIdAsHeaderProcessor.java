/*
 * BlueShift intelectual property.
 * Contact the BlueShift team before use this source.
 * http://blueshift.com.br/license
 */
package br.com.bs.processors;

import br.com.bs.config.NiFiRESTConstant;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.nifi.web.api.entity.ProcessorEntity;

/**
 *
 * @author André Camillo
 */
public class SetProcessorIdAsHeaderProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        ProcessorEntity processor
                = exchange.getIn().getBody(ProcessorEntity.class);
        processor.setStatus(null);

        exchange.getIn().setBody(processor);

        exchange.getIn().setHeader(
                NiFiRESTConstant.HEADER_PROCESSOR_ID, processor.getId());
    }

}
