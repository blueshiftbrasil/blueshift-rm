/*
 * BlueShift intelectual property.
 * Contact the BlueShift team before use this source.
 * http://blueshift.com.br/license
 */
package br.com.bs.processors;

import br.com.bs.config.NiFiRESTConstant;
import br.com.bs.model.FlowEntity;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.nifi.web.api.entity.ProcessGroupEntity;

/**
 *
 * @author André Camillo
 */
public class SetProcessGroupIdAsHeaderProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        FlowEntity flow = exchange.getIn().getHeader(
                NiFiRESTConstant.HEADER_FLOW_ENTITY, FlowEntity.class);

        ProcessGroupEntity processGroup
                = exchange.getIn().getBody(ProcessGroupEntity.class);

        flow.setFlowId(processGroup.getId());

        exchange.getIn().setHeader(
                NiFiRESTConstant.HEADER_FLOW_ENTITY, flow);

        exchange.getIn().setHeader(
                NiFiRESTConstant.HEADER_PROCESSGROUP_ID, processGroup.getId());

        exchange.getIn().setHeader(NiFiRESTConstant.RESOURCE_ID, processGroup.getId());
    }

}
