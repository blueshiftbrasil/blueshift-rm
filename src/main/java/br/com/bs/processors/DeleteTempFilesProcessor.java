/*
 * BlueShift intelectual property.
 * Contact the BlueShift team before use this source.
 * http://blueshift.com.br/license
 */
package br.com.bs.processors;

import br.com.bs.config.NiFiRESTConstant;
import br.com.bs.util.MultipartUtil;
import java.util.Map;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author André Camillo
 */
public class DeleteTempFilesProcessor implements Processor {

    public static final Logger LOGGER
            = LoggerFactory.getLogger(DeleteTempFilesProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        Map<String, String> files
                = exchange.getIn().getHeader(NiFiRESTConstant.MULTIPART_CACHE, Map.class);

        MultipartUtil.clearCache(files);
    }

}
