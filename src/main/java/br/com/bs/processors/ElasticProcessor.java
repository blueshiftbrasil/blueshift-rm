/*
 * BlueShift intelectual property.
 * Contact the BlueShift team before use this source.
 * http://blueshift.com.br/license
 */
package br.com.bs.processors;

import br.com.bs.config.AppConfig;
import br.com.bs.config.DefaultConstants;
import br.com.bs.config.NiFiRESTConstant;
import br.com.bs.exceptions.SearchException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import br.com.bs.config.ElasticConstants;
import br.com.bs.exceptions.UploadException;
import br.com.bs.model.GenericResponseEntity;
import br.com.bs.util.ResponseUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.search.SearchHit;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author André Camillo
 */
@Component
public class ElasticProcessor implements Processor {

    @Autowired
    private AppConfig config;

    public static final Logger LOGGER
            = LoggerFactory.getLogger(ElasticProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        String mode = exchange.getIn().getHeader(ElasticConstants.MODE, String.class);
        String id = exchange.getIn().getHeader(NiFiRESTConstant.RESOURCE_ID, String.class);
        Map filters = exchange.getIn().getHeader(NiFiRESTConstant.RESOURCES_SEARCH, Map.class);
        String elasticType = exchange.getIn().getHeader(ElasticConstants.ELASTIC_SEARCH_TYPE, String.class);
        
        String body = exchange.getIn().getBody(String.class);
        
        switch (mode) {
            case ElasticConstants.MODE_UPSERT:
                exchange.getIn().setBody(upsert(elasticType, body, id));
                break;

            case ElasticConstants.MODE_SEARCH_BY_ID:
                exchange.getIn().setBody(search(elasticType, id));
                break;
                
            case ElasticConstants.MODE_SEARCH:
                exchange.getIn().setBody(search(elasticType, this.notNullMap(filters)));
                break;

            case ElasticConstants.MODE_DELETE:
                exchange.getIn().setBody(delete(elasticType, id));
                break;
        }

        exchange.getIn().setHeader(Exchange.CONTENT_TYPE, DefaultConstants.CONTENT_TYPE_JSON);
    }
    
    private Map notNullMap(Map filters) {
        if (filters != null) {
            return filters;
        }
        
        return Collections.EMPTY_MAP;
    }

    private String upsert(String elasticType, String body, String id) throws UnknownHostException, InterruptedException, ExecutionException {
        try {
            try (TransportClient client = getClient()) {
                IndexRequest indexRequest = new IndexRequest(
                        ElasticConstants.ELASTIC_SEARCH_INDEX, elasticType, id)
                        .source(body);
                
                UpdateRequest updateRequest = new UpdateRequest(
                        ElasticConstants.ELASTIC_SEARCH_INDEX, elasticType, id)
                        .doc(body)
                        .upsert(indexRequest);
                
                client.update(updateRequest).get();
            }

            return body;
        } catch (JSONException ex) {
            throw new UploadException("Corpo JSON inválido para alteração.");
        }
    }

    private String search(String elasticType, String id) throws UnknownHostException {
        try {
            String responseBody;
            try (TransportClient client = getClient()) {
                responseBody = client.prepareSearch(ElasticConstants.ELASTIC_SEARCH_INDEX)
                        .setTypes(elasticType)
                        .setQuery(QueryBuilders.termQuery(ElasticConstants.ELASTIC_SEARCH_ID, id))
                        .setSearchType(SearchType.DEFAULT)
                        .get()
                        .getHits()
                        .getAt(0)
                        .getSourceAsString();
            }

            return responseBody;
        } catch (ArrayIndexOutOfBoundsException ex) {
            throw new SearchException("Parâmetros não encontrados.");
        }
    }
    
    private List<Map<String, Object>> search(String elasticType, Map filters) throws UnknownHostException {
        try {
            List<Map<String, Object>> result = new ArrayList();
            try (TransportClient client = getClient()) {
                SearchRequestBuilder builder = 
                        client.prepareSearch(ElasticConstants.ELASTIC_SEARCH_INDEX)
                        .setTypes(elasticType)
                        .setSearchType(SearchType.DEFAULT);
                
                Set<Map.Entry<String, Object>> filtersM = filters.entrySet();
                filtersM.stream().forEach((entry) -> {
                    builder.setQuery(QueryBuilders.termQuery(
                            entry.getKey(), entry.getValue()));
                });
                
                Iterator<SearchHit> it = builder.get()
                        .getHits()
                        .iterator();
                
                while (it.hasNext()) {
                    SearchHit next = it.next();
                    result.add(next.getSource());
                }
                
            }

            return result;
        } catch (ArrayIndexOutOfBoundsException ex) {
            throw new SearchException("Parâmetros não encontrados.");
        }
    }

    private GenericResponseEntity delete(String elasticType, String id) throws UnknownHostException {
        try (TransportClient client = getClient()) {
            client.prepareDelete(ElasticConstants.ELASTIC_SEARCH_INDEX, elasticType, id)
                    .execute()
                    .actionGet();
        }

        return ResponseUtil
                .getResponse(true, "Template '" + id + "' removido com sucesso.", null);
    }

    private TransportClient getClient() throws UnknownHostException {
        return new PreBuiltTransportClient(getSettings())
                .addTransportAddress(new InetSocketTransportAddress(
                        InetAddress.getByName(config.getElasticHost()), config.getElasticPort()));
    }

    private Settings getSettings() {
        return Settings.builder()
                .put(ElasticConstants.ELASTIC_SEARCH_CLUSTER, config.getElasticCluster())
                .build();
    }

}